import '../../polyfills/array/findIndex';

export const contains = 'contains';
export const containsAny = 'containsAny';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    contains(predicate: (element: T, index: number, array: T[]) => boolean): boolean;
    containsAny(predicate: (a: T, b: T, index: number, array: T[]) => boolean, ...items: T[]): boolean;
  }
}

if (!Array.prototype[contains]) {
  Object.defineProperty(Array.prototype, contains, {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(predicate: (element: any, index: number, array: any[]) => boolean): boolean {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }

      const list: any[] = Object(this);

      return list.findIndex(predicate) !== -1;

    },
    writable: false,
  });
}

if (!Array.prototype[containsAny]) {
  Object.defineProperty(Array.prototype, containsAny, {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(predicate: (a: any, b: any, index: number, array: any[]) => boolean, ...items: any[]): boolean {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }

      const list: any[] = Object(this);
      // tslint:disable:no-bitwise
      const length = list.length >>> 0;

      for (const item of items) {
        for (let i = 0; i < length; i++) {
          if (predicate(list[i], item, i, list)) {
            return true;
          }
        }
      }

      return false;
    },
    writable: false,
  });
}

import '../../polyfills/array/findIndex';

export const remove = 'remove';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    merge(predicate: (a: T, b: T, index: number, array: T[]) => boolean, ...items: T[]): void;
  }
}

if (!Array.prototype[remove]) {
  Object.defineProperty(Array.prototype, remove, {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(predicate: (a: any, b: any, index: number, array: any[]) => boolean, ...items: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }

      const list: any[] = Object(this);

      for (const item of items) {
        const indexOf = list.findIndex(
          (element: any, index: number, array: any[]) => predicate(element, item, index, array));
        if (indexOf) {
          this.splice(indexOf, 1);
        }
      }

    },
    writable: false,
  });
}

import './contains';

export const merge = 'merge';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    merge(predicate: (a: T, b: T) => boolean, ...items: T[]): void;
  }
}

if (!Array.prototype[merge]) {
  Object.defineProperty(Array.prototype, merge, {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(predicate: (a: any, b: any) => boolean, ...items: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }

      const list: any[] = Object(this);

      for (const item of items) {
        if (!list.contains(predicate)) {
          this.push(item);
        }
      }

    },
    writable: false,
  });
}

import './contains';

export const equals = 'equals';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    equals(array: T[]): boolean;
  }
}

if (!Array.prototype[equals]) {
  Object.defineProperty(Array.prototype, equals, {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(array: any[]): boolean {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }

      const list: any[] = Object(this);

      for (const item of array) {
        if (!list.contains(item)) {
          return false;
        }
      }

      return true;

    },
    writable: false,
  });
}

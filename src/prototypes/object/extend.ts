import './clone';

export const extendObject = 'extend';
export const extendDeepObject = 'extendDeep';

declare global {
  // tslint:disable:interface-name
  interface Object {
    extend(...objects: any[]): void;
    extendDeep(...objects: any[]): void;
  }
}

if (!Object.prototype[extendObject]) {
  Object.defineProperty(Object.prototype, extendObject, {
    configurable: true,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(...objects: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Object.prototype.extend called on null or undefined');
      }

      const self = Object(this);

      for (const object of objects) {
        for (const key in object) {
          if (object.hasOwnProperty(key)) {
            if (!self.hasOwnProperty(key)) {
              self[key] = object[key];
            }
          }
        }
      }

    },
    writable: true,
  });
}

if (!Object.prototype[extendDeepObject]) {
  Object.defineProperty(Object.prototype, extendDeepObject, {
    configurable: true,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(...objects: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Object.prototype.extend called on null or undefined');
      }

      const self = Object(this);

      for (const object of objects) {
        for (const key in object) {
          if (object.hasOwnProperty(key)) {
            if (!self.hasOwnProperty(key)) {

              if (typeof object[key] === 'object') {
                self[key] = object[key].clone();
              } else {
                self[key] = object[key];
              }
            } else if (typeof object[key] === 'object' && typeof self[key] === 'object') {
              self[key].extendDeep(object[key]);
            }
          }
        }
      }

    },
    writable: true,
  });
}

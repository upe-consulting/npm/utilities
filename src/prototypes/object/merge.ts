import './clone';

export const mergeObject = 'merge';
export const mergeDeepObject = 'mergeDeep';

declare global {
  // tslint:disable:interface-name
  interface Object {
    merge(...objects: any[]): void;
    mergeDeep(...objects: any[]): void;
  }
}

if (!Object.prototype[mergeObject]) {
  Object.defineProperty(Object.prototype, mergeObject, {
    configurable: true,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(...objects: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Object.prototype.merge called on null or undefined');
      }

      const self = Object(this);

      for (const object of objects) {
        for (const key in object) {
          if (object.hasOwnProperty(key)) {
            self[key] = object[key];
          }
        }
      }

    },
    writable: true,
  });
}

if (!Object.prototype[mergeDeepObject]) {
  Object.defineProperty(Object.prototype, mergeDeepObject, {
    configurable: true,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(...objects: any[]): void {
      'use strict';
      if (this === null) {
        throw new TypeError('Object.prototype.extend called on null or undefined');
      }

      const self = Object(this);

      for (const object of objects) {
        for (const key in object) {
          if (object.hasOwnProperty(key)) {
            if (!self.hasOwnProperty(key)) {
              if (typeof object[key] === 'object') {
                self[key] = object[key].clone();
              } else {
                self[key] = object[key];
              }
            } else if (typeof object[key] === 'object' && typeof self[key] === 'object') {
              self[key].mergeDeep(object[key]);
            } else {
              self[key] = object[key];
            }
          }
        }
      }

    },
    writable: true,
  });
}

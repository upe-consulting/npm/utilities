export const cloneObject = 'clone';

declare global {
  // tslint:disable:interface-name
  interface Object {
    clone(): any;
  }
}

if (!Object.prototype[cloneObject]) {
  Object.defineProperty(Object.prototype, cloneObject, {
    configurable: true,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(): any {
      'use strict';
      if (this === null) {
        throw new TypeError('Object.prototype.clone called on null or undefined');
      }

      const self = Object(this);

      return JSON.parse(JSON.stringify(self));

    },
    writable: true,
  });
}

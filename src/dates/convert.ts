import * as moment from 'moment';

import {GetLocaleName} from '../helper';

export function StringToDate(
  date: string,
  formats: string[] = [],
  locale: string = GetLocaleName(),
  strict: boolean = false,
  utc: boolean = true
): Date | null {
  const momentDate = moment(date, formats, locale, strict);

  return momentDate ? utc ? momentDate.utc().toDate() : momentDate.toDate() : null;
}

export function DateToString(date: Date, format: string = 'YYYY-MM-DDTHH:mm:ss.SSS[Z]', utc: boolean = true): string {
  let dateString: string;

  if (utc) {
    dateString = moment(date).utc().format(format);
  } else {
    dateString = moment(date).format(format);
  }

  return dateString;
}

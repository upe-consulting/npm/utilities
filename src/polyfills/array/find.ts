export const find = 'find';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    find(predicate: (element: T, index: number, array: T[]) => boolean, thisArg?: any): T | null;
  }
}

if (!Array.prototype[find]) {
  Array.prototype[find] = function(
    predicate: (element: any, index: number, array: any[]) => boolean,
    thisArg?: any
  ): any | null {
    'use strict';
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    const list = Object(this);
    // tslint:disable:no-bitwise
    const length = list.length >>> 0;
    let value;

    for (let i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }

    return null;
  };
}

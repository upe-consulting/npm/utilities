export const findIndex = 'findIndex';

declare global {
  // tslint:disable:interface-name
  interface Array<T> {
    findIndex(predicate: (element: T, index: number, array: T[]) => boolean, thisArg?: any): number;
  }
}

if (!Array.prototype[findIndex]) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    configurable: false,
    enumerable: false,
    // tslint:disable:object-literal-shorthand
    value: function(predicate: (element: any, index: number, array: any[]) => boolean, thisArg?: any): number {
      'use strict';
      if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
      }
      const list = Object(this);
      // tslint:disable:no-bitwise
      const length = list.length >>> 0;
      let value;

      for (let i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
          return i;
        }
      }

      return -1;
    },
    writable: false,
  });
}

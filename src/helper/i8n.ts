const languages = 'languages';

export function GetLocaleName(): string {
  return navigator.hasOwnProperty(languages) && navigator[languages]
    ? navigator[languages][0]
    : navigator.language;
}

export function ArrayFactory<T>(factory: (...args: any[]) => T, count: number = 10, ...args: any[]): T[] {
  const objs: T[] = [];
  for (let i = 0; i < count; i++) {
    objs.push(factory(args));
  }

  return objs;
}

import {ArrayFactory} from './';

export function GetRandomNumberArray(min: number = 0, max: number = 100, count: number = 10): number[] {
  return ArrayFactory(() => Math.floor(Math.random() * (
      max - min
    )) + min, count);
}

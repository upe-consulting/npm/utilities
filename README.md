[![build status](https://gitlab.com/upe-consulting/npm/utilities/badges/master/build.svg)](https://gitlab.com/upe-consulting/npm/utilities/commits/master)
[![coverage report](https://gitlab.com/upe-consulting/npm/utilities/badges/master/coverage.svg)](https://gitlab.com/upe-consulting/npm/utilities/commits/master)

## Changelog

### 0.2.0
+ add prototypes
    + Object.clone
    + Object.merge
    + Object.mergeDeep
    + Object.extend
    + Object.extendDeep

### 0.1.0
+ add polyfills
    + Array.find
    + Array.findIndex
+ add prototypes
    + Array.contains
    + Array.equals
    + Array.merge
    + Array.remove

### 0.0.1
+ add GetLocaleName()
+ add StringToDate()
+ add DateToString()
import {expect} from 'chai';
import {suite, test} from 'mocha-typescript';

import {mergeObject} from '../../../src/prototypes/object/merge';

@suite
class MergeObjectTest {

  @test
  public 'exists prototype'(): void {

    expect(Object.prototype[mergeObject]).to.be.not.undefined;

  }

  @test
  public 'trival merge'(): void {

    const object0 = {};
    const object1 = {};

    object0.merge(object1);

    expect(object0).to.deep.equal(object1);

  }

  @test
  public 'single merge'(): void {
    const object0 = {};
    const object1 = {testKey: 'testVal'};

    object0.merge(object1);

    expect(object0).to.deep.equal(object1);

    const object2 = {testKey: 'newTestVal'};

    object0.merge(object2);

    expect(object0).to.deep.equal(object2);

  }

  @test
  public 'singel function merge'(): void {

    const object0 = {};
    const object1 = {testKey: () => {}};

    object0.merge(object1);

    expect(object0).to.deep.equal(object1);

  }

  @test
  public 'multi merge'(): void {

    const object0 = {};
    const object1 = {testKey: 'testVal'};
    const object2 = {testKey: 'tmpVal'};
    const object3 = {testKey: 'newVal', testKey1: 'testVal'};

    object0.merge(object1, object2, object3);

    expect(object0).to.deep.equal(object3);

  }

  @test
  public 'single deep merge'(): void {

    const orignal = {
      testKey: 'testVal',
      testObj: {
        testKey: 'testVal'
      }
    };

    const object0 = orignal.clone();

    const object1 = {
      testKey: 'newVal',
      testObj: 'testVal'
    };

    object0.mergeDeep(object1);

    expect(object0).to.deep.equal(object1);

    const object2 = {
      testKey: 'testVal',
      testObj: {
        testKey: 'newVal'
      }
    };

    object0.mergeDeep(object2);

    expect(object0).to.deep.equal(object2);

    const object3 = {
      testKey: 'testVal',
      testObj: {
        testKey: 'newVal',
        newKey: 'testVal'
      },
      newObj: {
        testKey: 'testKey'
      }
    };

    object0.mergeDeep(object3);

    expect(object0).to.deep.equal(object3);

  }

}

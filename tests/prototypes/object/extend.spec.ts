import {expect} from 'chai';
import {suite, test} from 'mocha-typescript';

import {extendObject} from '../../../src/prototypes/object/extend';
import '../../../src/prototypes/object/clone';

@suite
class ExtendObjectTest {

  @test
  public 'exists prototype'(): void {

    expect(Object.prototype[extendObject]).to.be.not.undefined;

  }

  @test
  public 'tivial extend'(): void {

    const object0 = {};
    const object1 = {};

    object0.extend(object1);

    expect(object0).to.deep.equal({});

  }

  @test
  public 'single extend'(): void {
    const object0 = {};
    const object1 = {testKey: 'testVal'};

    object0.extend(object1);

    expect(object0).to.deep.equal(object1);

    const object2 = {testKey: 'newTestVal'};

    object0.extend(object2);

    expect(object0).to.deep.equal(object1);

  }

  @test
  public 'singel function extend'(): void {

    const object0 = {};
    const object1 = {testKey: () => {}};

    object0.extend(object1);

    expect(object0).to.deep.equal(object1);

  }

  @test
  public 'multi extend'(): void {

    const object0 = {};
    const object1 = {testKey: 'testVal'};
    const object2 = {testKey: 'newVal'};
    const object3 = {testKey: 'testVal', testKey1: 'testVal'};

    object0.extend(object1, object2, object3);

    expect(object0).to.deep.equal(object3);

  }

  @test
  public 'single deep extend'(): void {

    const orignal = {
      testKey: 'testVal',
      testObj: {
        testKey: 'testVal'
      }
    };

    const object0 = orignal.clone();

    const object1 = {
      testKey: 'newVal',
      testObj: 'testVal'
    };

    object0.extendDeep(object1);

    expect(object0).to.deep.equal(orignal);

    const object2 = {
      testObj: {
        testKey: 'newVal'
      }
    };

    object0.extendDeep(object2);

    expect(object0).to.deep.equal(orignal);

    const object3 = {
      testKey: 'testVal',
      testObj: {
        testKey: 'testVal',
        newKey: 'testVal'
      },
      newObj: {
        testKey: 'testKey'
      }
    };

    object0.extendDeep(object3);

    expect(object0).to.deep.equal(object3);

  }

}
import {expect} from 'chai';
import {suite, test} from 'mocha-typescript';
import {DateToString} from '../../src/dates/convert';

@suite
class ConvertDateTest {

  @test
  public 'date to string'(): void {

    const date = new Date(Date.now());

    const dateString = DateToString(date);

    expect(dateString).eq(date.toISOString());

  }

}
